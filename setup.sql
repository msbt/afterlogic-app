-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `au_eav_attributes_bigint`
--

DROP TABLE IF EXISTS `au_eav_attributes_bigint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `au_eav_attributes_bigint` (
  `id` bigint(64) unsigned NOT NULL AUTO_INCREMENT,
  `id_entity` bigint(64) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` bigint(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`id_entity`,`name`),
  KEY `idx_value` (`value`),
  KEY `idx_name` (`name`),
  KEY `fk_id_entity_idx` (`id_entity`),
  CONSTRAINT `au_fk_eav_attributes_bigint_id_entity` FOREIGN KEY (`id_entity`) REFERENCES `au_eav_entities` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `au_eav_attributes_bigint`
--

LOCK TABLES `au_eav_attributes_bigint` WRITE;
/*!40000 ALTER TABLE `au_eav_attributes_bigint` DISABLE KEYS */;
/*!40000 ALTER TABLE `au_eav_attributes_bigint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `au_eav_attributes_bool`
--

DROP TABLE IF EXISTS `au_eav_attributes_bool`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `au_eav_attributes_bool` (
  `id` bigint(64) unsigned NOT NULL AUTO_INCREMENT,
  `id_entity` bigint(64) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`id_entity`,`name`),
  KEY `idx_value` (`value`),
  KEY `idx_name` (`name`),
  KEY `fk_id_entity_idx` (`id_entity`),
  CONSTRAINT `au_fk_eav_attributes_bool_id_entity` FOREIGN KEY (`id_entity`) REFERENCES `au_eav_entities` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `au_eav_attributes_bool`
--

LOCK TABLES `au_eav_attributes_bool` WRITE;
/*!40000 ALTER TABLE `au_eav_attributes_bool` DISABLE KEYS */;
INSERT INTO `au_eav_attributes_bool` VALUES (1,2,'IsDisabled',0),(2,2,'IsDefault',0),(5,3,'IncomingUseSsl',1),(6,3,'EnableSieve',1);
/*!40000 ALTER TABLE `au_eav_attributes_bool` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `au_eav_attributes_datetime`
--

DROP TABLE IF EXISTS `au_eav_attributes_datetime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `au_eav_attributes_datetime` (
  `id` bigint(64) unsigned NOT NULL AUTO_INCREMENT,
  `id_entity` bigint(64) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`id_entity`,`name`),
  KEY `idx_value` (`value`),
  KEY `idx_name` (`name`),
  KEY `fk_id_entity_idx` (`id_entity`),
  CONSTRAINT `au_fk_eav_attributes_datetime_id_entity` FOREIGN KEY (`id_entity`) REFERENCES `au_eav_entities` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `au_eav_attributes_datetime`
--

LOCK TABLES `au_eav_attributes_datetime` WRITE;
/*!40000 ALTER TABLE `au_eav_attributes_datetime` DISABLE KEYS */;
/*!40000 ALTER TABLE `au_eav_attributes_datetime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `au_eav_attributes_double`
--

DROP TABLE IF EXISTS `au_eav_attributes_double`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `au_eav_attributes_double` (
  `id` bigint(64) unsigned NOT NULL AUTO_INCREMENT,
  `id_entity` bigint(64) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`id_entity`,`name`),
  KEY `idx_name` (`name`),
  KEY `fk_id_entity_idx` (`id_entity`),
  CONSTRAINT `au_fk_eav_attributes_double_id_entity` FOREIGN KEY (`id_entity`) REFERENCES `au_eav_entities` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `au_eav_attributes_double`
--

LOCK TABLES `au_eav_attributes_double` WRITE;
/*!40000 ALTER TABLE `au_eav_attributes_double` DISABLE KEYS */;
/*!40000 ALTER TABLE `au_eav_attributes_double` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `au_eav_attributes_int`
--

DROP TABLE IF EXISTS `au_eav_attributes_int`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `au_eav_attributes_int` (
  `id` bigint(64) unsigned NOT NULL AUTO_INCREMENT,
  `id_entity` bigint(64) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`id_entity`,`name`),
  KEY `idx_value` (`value`),
  KEY `idx_name` (`name`),
  KEY `fk_id_entity_idx` (`id_entity`),
  CONSTRAINT `au_fk_eav_attributes_int_id_entity` FOREIGN KEY (`id_entity`) REFERENCES `au_eav_entities` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `au_eav_attributes_int`
--

LOCK TABLES `au_eav_attributes_int` WRITE;
/*!40000 ALTER TABLE `au_eav_attributes_int` DISABLE KEYS */;
INSERT INTO `au_eav_attributes_int` VALUES (1,2,'IdChannel',1),(3,3,'TenantId',0),(4,3,'IncomingPort',993),(5,3,'OutgoingPort',587);
/*!40000 ALTER TABLE `au_eav_attributes_int` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `au_eav_attributes_mediumblob`
--

DROP TABLE IF EXISTS `au_eav_attributes_mediumblob`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `au_eav_attributes_mediumblob` (
  `id` bigint(64) unsigned NOT NULL AUTO_INCREMENT,
  `id_entity` bigint(64) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` mediumblob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`id_entity`,`name`),
  KEY `idx_name` (`name`),
  KEY `fk_id_entity_idx` (`id_entity`),
  CONSTRAINT `au_fk_eav_attributes_mediumblob_id_entity` FOREIGN KEY (`id_entity`) REFERENCES `au_eav_entities` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `au_eav_attributes_mediumblob`
--

LOCK TABLES `au_eav_attributes_mediumblob` WRITE;
/*!40000 ALTER TABLE `au_eav_attributes_mediumblob` DISABLE KEYS */;
/*!40000 ALTER TABLE `au_eav_attributes_mediumblob` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `au_eav_attributes_string`
--

DROP TABLE IF EXISTS `au_eav_attributes_string`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `au_eav_attributes_string` (
  `id` bigint(64) unsigned NOT NULL AUTO_INCREMENT,
  `id_entity` bigint(64) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`id_entity`,`name`),
  KEY `idx_value` (`value`),
  KEY `idx_name` (`name`),
  KEY `fk_id_entity_idx` (`id_entity`),
  CONSTRAINT `au_fk_eav_attributes_string_id_entity` FOREIGN KEY (`id_entity`) REFERENCES `au_eav_entities` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `au_eav_attributes_string`
--

LOCK TABLES `au_eav_attributes_string` WRITE;
/*!40000 ALTER TABLE `au_eav_attributes_string` DISABLE KEYS */;
INSERT INTO `au_eav_attributes_string` VALUES (1,1,'Login','Default'),(2,1,'Password','79c82a4ad8ef555bb4f392140c23d20f'),(3,2,'Name','Default'),(5,3,'Name','cloudron'),(6,3,'IncomingServer','my.MAILDOMAIN_IN'),(7,3,'OutgoingServer','my.MAILDOMAIN_OUT'),(8,3,'SmtpAuthType','2'),(9,3,'OwnerType','superadmin');
/*!40000 ALTER TABLE `au_eav_attributes_string` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `au_eav_attributes_text`
--

DROP TABLE IF EXISTS `au_eav_attributes_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `au_eav_attributes_text` (
  `id` bigint(64) unsigned NOT NULL AUTO_INCREMENT,
  `id_entity` bigint(64) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`id_entity`,`name`),
  KEY `idx_name` (`name`),
  KEY `fk_id_entity_idx` (`id_entity`),
  CONSTRAINT `au_fk_eav_attributes_text_id_entity` FOREIGN KEY (`id_entity`) REFERENCES `au_eav_entities` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `au_eav_attributes_text`
--

LOCK TABLES `au_eav_attributes_text` WRITE;
/*!40000 ALTER TABLE `au_eav_attributes_text` DISABLE KEYS */;
INSERT INTO `au_eav_attributes_text` VALUES (1,3,'Domains','*');
/*!40000 ALTER TABLE `au_eav_attributes_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `au_eav_entities`
--

DROP TABLE IF EXISTS `au_eav_entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `au_eav_entities` (
  `id` bigint(64) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) NOT NULL,
  `parent_uuid` char(36) NOT NULL,
  `entity_type` varchar(255) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_uuid` (`uuid`),
  KEY `idx_parent_uuid` (`parent_uuid`),
  KEY `idx_entity_type` (`entity_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `au_eav_entities`
--

LOCK TABLES `au_eav_entities` WRITE;
/*!40000 ALTER TABLE `au_eav_entities` DISABLE KEYS */;
INSERT INTO `au_eav_entities` VALUES (1,'0fd8f8cb-9662-499b-b1f8-b744f364ba76','','Aurora\\Modules\\Core\\Classes\\Channel','Core'),(2,'3a8c39e5-4f21-46bb-be0a-fe23145f2781','','Aurora\\Modules\\Core\\Classes\\Tenant','Core'),(3,'e652f858-a577-4f08-a6ad-aaf74eb8f9b0','','Aurora\\Modules\\Mail\\Classes\\Server','Mail');
/*!40000 ALTER TABLE `au_eav_entities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `au_min_hashes`
--

DROP TABLE IF EXISTS `au_min_hashes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `au_min_hashes` (
  `hash_id` varchar(32) NOT NULL DEFAULT '',
  `hash` varchar(20) NOT NULL DEFAULT '',
  `data` text,
  KEY `au_min_hash_index` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `au_min_hashes`
--

LOCK TABLES `au_min_hashes` WRITE;
/*!40000 ALTER TABLE `au_min_hashes` DISABLE KEYS */;
/*!40000 ALTER TABLE `au_min_hashes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-19 20:09:25

