FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

ENV VERSION=8

RUN mkdir -p /app/code/ /app/data
WORKDIR /app/code

# get afterlogic and extract it
RUN wget https://afterlogic.org/download/webmail-lite-php-${VERSION}.zip && \
    unzip webmail-lite-php-${VERSION}.zip && rm webmail-lite-php-${VERSION}.zip && \
    mv /app/code/.htaccess /app/code/.htaccess_orig && \
    ln -sf /app/data/.htaccess /app/code/.htaccess && \
    mv /app/code/data /app/code/data_orig && \
    ln -sf /app/data/data /app/code/data

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/afterlogic.conf /etc/apache2/sites-enabled/afterlogic.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod rewrite mime ldap authnz_ldap
RUN crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_filesize 128M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_size 128M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP post_max_size 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.save_path /run/sessions && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_divisor 100

ADD start.sh /app/
ADD config.json_tpl /app/code/data_orig/
ADD setup.sql /app/code/data_orig/

RUN chown -R www-data.www-data /app/code /app/data

CMD [ "/app/start.sh" ]
