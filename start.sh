#!/bin/bash

set -eu

if [[ ! -d /app/data/data ]]; then
    echo "=> Detected first run"

    cp /app/code/.htaccess_orig /app/data/.htaccess
    cp -R /app/code/data_orig /app/data/data
    chmod -R 0777 /app/data/data
    cp /app/code/data_orig/config.json_tpl /app/data/data/settings/config.json

    # update mysql settings
    sed -i "s/MYSQL_SERVER/${MYSQL_HOST}/" /app/data/data/settings/config.json
    sed -i "s/MYSQL_DB/${MYSQL_DATABASE}/" /app/data/data/settings/config.json
    sed -i "s/MYSQL_USER/${MYSQL_USERNAME}/" /app/data/data/settings/config.json
    sed -i "s/MYSQL_PASSWORD/${MYSQL_PASSWORD}/" /app/data/data/settings/config.json
    sed -i "s/MAILDOMAIN_IN/${MAIL_DOMAIN}/" /app/data/data/setup.sql
    sed -i "s/MAILDOMAIN_OUT/${MAIL_DOMAIN}/" /app/data/data/setup.sql

    #import db
    mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQL_DATABASE} < /app/data/data/setup.sql

    # cleanup
    rm /app/data/data/setup.sql
    rm /app/data/data/config.json_tpl
fi


echo "=> Ensuring runtime directories"
chown -R www-data.www-data /app/data

echo "=> Updating DB-Data in case it changed"
sed -i "s/MYSQL_SERVER/${MYSQL_HOST}/" /app/data/data/settings/config.json
sed -i "s/MYSQL_DB/${MYSQL_DATABASE}/" /app/data/data/settings/config.json
sed -i "s/MYSQL_USER/${MYSQL_USERNAME}/" /app/data/data/settings/config.json
sed -i "s/MYSQL_PASSWORD/${MYSQL_PASSWORD}/" /app/data/data/settings/config.json


echo "=> Run afterlogic"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
